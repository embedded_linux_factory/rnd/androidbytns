# Run android-rpi image with qemu

After building the `android-rpi` project we will try to run the it with qemu.

## Creating a SD card Image for Qemu

In order to emulate the project, we will first emulate a virtual SD card to install the built Android image.

For that, we can use qemu to create an image:

```sh
qemu-img create ./sdcard.img 32768M
```

> Note, Here we created an image of 32 Gb, which is the recommended size to run the android-rpi project.

Then, to access the created image, we need to link it to a network block device.

```sh
qemu-nbd -f raw ./sdcard.img &
```

After that, to access the network block device, we need a client to communicate with it. For that, on **Debian 12**, we can install the following package:

```sh
sudo apt-get install nbd-client
```

Then we will need to load the associated kernel module with:

```sh
sudo modprobe nbd
```

Finally, we can connect to the disk with:

```sh
sudo nbd-client localhost
```

## Formatting the disk image

Now that we created a virtual SD car, we need to partition it. For that, we will create the following partition scheme for the SD card image:

![01](images/01.png)

## Copy the Android images to the virtual SD card

You first need to copy the system and verdor partitions to the SD card:

```sh
sudo dd if=system.img of=/dev/<p2> bs=1M
sudo dd if=vendor.img of=/dev/<p3> bs=1M
```

You should get an output like the following:

![02](images/02.png)

Then to copy the kernel image on the bootable partition, we need to install the following package:

```sh
sudo apt-get install dosfstools
```

After that, you can create a mount point and copy the following files to the bootable partition:

```sh
sudo mkdir /mnt/p1
sudo mount -t vfat /dev/nbd0p1 /mnt/p1
sudo cp aosp/device/brcm/rpi3/boot/* /mnt/p1
sudo cp kernel/rpi/arch/arm/boot/zImage /mnt/p1
sudo cp kernel/rpi/arch/arm/boot/dts/bcm2710-rpi-3-b.dtb /mnt/p1
sudo cp kernel/rpi/arch/arm/boot/dts/overlays/vc4-kms-v3d.dtbo /mnt/p1/overlays/vc4-kms-v3d.dtbo
sudo cp out/target/product/rpi3/ramdisk.img /mnt/p1
```

## Run the Android image with qemu

Once the virtual sdcard is correctly formatted, you can run the image with the following qemu command:

```sh
qemu-system-aarch64 \
    -machine raspi3b \
    -cpu cortex-a72 \
    -m 1G \
    -smp 4 \
    -drive file="$RASPIOS_IMG",if=sd,format=raw \
    -nographic
```

> Note: For now the image cannot run due to the following error:
> ```
> Taking exception 1 [Undefined Instruction] on CPU 1
> ...from EL3 to EL3
> ...with ESR 0x0/0x2000000
> ...with ELR 0x200
> ...to EL3 PC 0x200 PSTATE 0x3cd
> ```

# Liens

[How To Create SD Card Image For Qemu Emulator](https://github.com/wayling/xboot-clone/blob/master/documents/howto/How%20To%20Create%20SD%20Card%20Image%20For%20Qemu%20Emulator)
