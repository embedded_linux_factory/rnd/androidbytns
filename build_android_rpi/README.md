# Build android rpi

In order to build an Android image we will create a **docker container** to set up the build environement.

## Folder architecture

This folder is composed of the following files:

- [clean-up.sh](clean-up.sh): Remove the created image and container
- [docker-compose.yml](docker-compose.yml): Build the docker image and run the container `android_rpi_build_image`
- [android_rpi_build_image/Dockerfile](android_rpi_build_image/Dockerfile): Configure the build image
- [android_rpi_build_image/gcc-linaro-7.4.1-2019.02-x86_64_arm-linux-gnueabihf.tar.xz](android_rpi_build_image/gcc-linaro-7.4.1-2019.02-x86_64_arm-linux-gnueabihf.tar.xz): Required toolchain to build the Android image for Raspberry Pi
- [android_rpi_build_image/gitconfig](android_rpi_build_image/gitconfig): Example of git configuration that is required to initialize repo
- [android_rpi_build_image/install_packages.sh](android_rpi_build_image/install_packages.sh): Install the required packages to build the image
- [android_rpi_build_image/install_repo.sh](android_rpi_build_image/install_repo.sh): Install repo

## Run the build container

You can run the container as a daemon with docker compose:

```sh
docker compose up -d
```

Then you can access it by launching a shell attached to it with docker exec:

```sh
docker exec -it android_rpi_build_image /bin/bash
```

## Get the AOSP sources

To get the AOSP sources we need to use the repo tool from the Google's developpers which is installed in the container.

First, go to the aosp folder which is available in the `/root` folder:

```sh
cd /root/aosp
```

This folder is linked to a docker volume in which we will store the aosp sources and the image once the build process is finised.

You can initialize the repository with repo:

```sh
repo init -u https://android.googlesource.com/platform/manifest -b android-10.0.0_r32
```

After the initialization, you can clone the **build manifest** from `android-rpi` project:

```sh
git clone https://github.com/android-rpi/local_manifests .repo/local_manifests -b android10
```

Finally, you can get the Android sources with:

```sh
repo sync
```

> Note: fetching the sources can take around 1 hour.

## Building the kernel image

In order to build the kernel image, you can go to the `kernel/rpi` folder in the `aosp` sources:

```sh
cd kernel/rpi
```

Then you can merge existing kernel configurations:

```sh
ARCH=arm scripts/kconfig/merge_config.sh arch/arm/configs/bcm2709_defconfig kernel/configs/android-base.config kernel/configs/android-recommended.config ~/.config
```

If a `.config` hasn't been generated, you can create one with the following command:

```sh
make oldconfig
```

After the configurations have been correctly merged, you can build the kernel image named `zImage`:

```sh
ARCH=arm CROSS_COMPILE=arm-linux-gnueabihf- make zImage
```

### Fixing build errors

When building the kernel image, you may encounter the following errors:

#### `yylloc` redefinition

If you encouter an error caused by the redefinition of the symbol `yylloc`, you can declare the symbol as extern in the file `kernel/rpi/scripts/dtc/dtc-lexer.l`.

![01](images/build_android_rpi/01.png)

![02](images/build_android_rpi/01.png)

#### implicit delaration of `of_property_count_strings`

If you encounter this error, you can include the header `<linux/of.h>` in the right file.

#### implicit declaration of  `generic_handle_irq`

If you encounter this error, you can include the header `<linux/irq.h>` in the right file.

After the compilation succeed, the created binary will be stored at `kernel/rpi/arch/arm/boot/zImage`

Finally, you can build the **device tree sources** from the **device tree blob**:

```sh
ARCH=arm CROSS_COMPILE=arm-linux-gnueabihf- make dtbs
```

## Build the system image

To build the **system image**, you can follow the instruction from android docs.

First, in the `aosp` source folder, initialize the build environment:

```sh
source build/envsetup.sh
```

Then you can select the target architecture:

```sh
lunch rpi3-eng
```

Finally, you can build the system images:

```sh
make ramdisk systemimage vendorimage
```

> Note: Once the build process is done, you can try to run image by following the steps described in [qemu.md](qemu.md) or [run_rpi.md](run_rpi.md).

## Stop the build container

After the build is done, you can run the script `clean-up.sh` to stop and remove the build container:

```sh
./clean-up.sh
```

## Liens

[android-rpi](https://github.com/android-rpi/device_brcm_rpi3)

[Debug](https://github.com/BPI-SINOVOIP/BPI-M4-bsp/issues/4)

[LKML](https://lkml.org/lkml/2020/4/1/1206)

[Coloured splash screen](https://elinux.org/R-Pi_Troubleshooting#Coloured_splash_screen)