#!/bin/sh

# Interrupt script in case of error
set -euo pipefail

# Define image name
BUILD_IMAGE="android_rpi_build_image"

# Stop and remove running docker container
docker container stop "$BUILD_IMAGE"
docker container rm "$BUILD_IMAGE"

# Remove created image
docker rmi "$BUILD_IMAGE"
