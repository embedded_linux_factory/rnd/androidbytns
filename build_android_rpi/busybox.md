# Get a busybox image to run on the Android image

## Download busybox

To Download busybox, you can follow this link: https://www.busybox.net/downloads/busybox-1.36.1.tar.bz2

## Cross compile busybox for arm

In order to cross compile busybox for an arm architecture, we first need to install our cross compilation toolchain, `gcc-arm-linux-gnueabi`. To do that, we can use apt on Debian:

```sh
sudo apt install gcc-arm-linux-gnueabi
```

Then we will define the busybox configuration by using the following command:

```sh
make ARCH=arm CROSS_COMPILE=arm-linux-gnueabi- defconfig
```

This command will generate a build configuration in the file `.config`. To make sure that our busybox binary will be statically linked, we will add the following option in the `.config` file:

```sh
# .config
CONFIG_STATIC=y
```

Then we will compile the busybox binary with the following command:

```sh
make ARCH=arm CROSS_COMPILE=arm-linux-gnueabi- -j $(nproc)
```

We will then have a busybox binary ready in our busybox folder.

## Edit initrd to load the busybox binary

In the Android architecture, we are using the `initrd` protocol in order to initalize the environment to run the init process. What we want to acheive now is to run our busybox binary as an init script in order to manually run the Android boot process and see what is crashing.

For that, in a separate folder, we will extract the initial ramdisk `ramdisk.img`:

```sh
mkdir ~/ramdisk && cd ~/ramdisk
cp $(ANDROID_BOOT_FOLDER)/ramdisk.img ~/ramdisk
zcat ramdisk.img | cpio -idmv
```

You should get the following file architecture:

```
.
├── apex
├── debug_ramdisk
├── dev
├── fstab.rpi3
├── init
├── mnt
├── proc
├── ramdisk.img
└── sys
```

After we get the content of the initial ramdisk, we can add our busybox binary into the initial ramdisk:

```sh
cp $(BUSYBOX_WORKING_DIR)/busybox $(RAMDISK_WORKING_DIR)
```

Then we will repack the archive by making sure that we don't add the original `ramdisk.img` to it:

```sh
rm ramdisk.img
find . | cpio -o -c -R root:root | gzip -9 > ./ramdisk.img
```

Then we will tell our Android kernel to invoke it at start up by editing the kernel command line `cmdline.txt`:

```sh
echo " init=busybox" >> $(ANDROID_BOOT_PARTITION)/cmdline.txt
```