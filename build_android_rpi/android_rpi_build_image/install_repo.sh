#!/bin/bash

# interrupt script if an error occur
set -euo pipefail

# Create empty file to be replaced by the binary
mkdir ~/.bin
touch ~/.bin/repo

# Get repo from the google's sources
export REPO=$(mktemp /tmp/repo.XXXXXXXXX)
curl -o ${REPO} https://storage.googleapis.com/git-repo-downloads/repo
gpg --recv-keys 8BB9AD793E8E6153AF0F9A4416530D5E920F5C65
curl -s https://storage.googleapis.com/git-repo-downloads/repo.asc | gpg --verify - ${REPO} && install -m 755 ${REPO} ~/.bin/repo

# Create a repo alias
echo -e "# repo\nalias repo='~/.bin/repo'" >> .bashrc
