#!/bin/bash

# Fail if any error occur
set -euo pipefail

apt-get update

apt-get upgrade

DEBIAN_FRONTEND=noninteractive \
  apt-get -y install bc \
                     bison \
                     curl \
                     dialog \
                     flex \
                     git \
                     gpg \
                     libncurses5 \
                     libssl-dev \
                     make \
                     procps \
                     python3-mako \
                     unzip \
                     vim \
                     xz-utils
