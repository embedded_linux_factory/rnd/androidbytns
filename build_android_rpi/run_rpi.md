# Run android-rpi

## Format the SD card

In order to run our Android image, we will need to flash it on an empty SD card. To do so, we will connect our SD card to our bulid computer and see that the SD card has been mouted on `/dev/sdb`. We can check what's is inside the partition with `fdisk`:

```sh
sudo fdisk -l /dev/sdb
```

![01](images/run_rpi/01.png)

We can see that there is already 1 partition that is mounted on `/dev/sdb1`. As we don't want to keep it, we can unmount it:

```sh
sudo umount /dev/sdb1
```

After we get an empty partition table, we will want to create the following partition scheme:

> TODO: clean that

```
p1 256MB for BOOT : Do fdisk : W95 FAT32(LBA) & Bootable, mkfs.vfat
p2 640MB for /system : Do fdisk, new primary partition
p3 128MB for /vendor : Do fdisk, new primary partition
p4 remainings for /data : Do fdisk, mkfs.ext4
```

In order to edit the partitions of the SD card, we will use `fdisk`:

```sh
sudo fdisk /dev/sdb
```

Then, to create the first partition, we will procced as follow:

![02](images/run_rpi/02.png)

Here we created a new **primary partition** with a size of **256Mb**, changed its type to `W95 FAT32` and marked it as **bootable**.

Then we will create 3 new primary parititions that will represent the **system**, **vendor** and **data** partitions.

![03](images/run_rpi/03.png)

![04](images/run_rpi/04.png)

After that, you should get the following partition table:

![05](images/run_rpi/05.png)

Once you make sure that you have the right partition table, you write the changes to the disk:

![06](images/run_rpi/06.png)

> Note: Once we have created the partition table, we can save it in a format we can reuse on other disk. For that, we will use the following command to dump the partition table in a reusable format:
>
> ```sh
> sudo sfdisk --dump /dev/sdb > sdb.dump
> ```
>
> And we will use the following command to retreive the partition table from the dumped file:
>
> ```sh
> sudo sfdisk /dev/sdb < sdb.dump
> ```
>
> In this repository, you will find the dumped partition table that was used in the project in the file [sdb.dump](sdb.dump).

## Create file systems

After we created the partition, we will create a vfat filesystem for the bootable partition and an ext4 filesystem for the data partition:

```sh
sudo mkfs.vfat /dev/sdb1
sudo mkfs.ext4 -L "userdata" -b 4096 /dev/sdb4
```

## Write partitions

### Write system and vendor partitions

To write the system and vendor partitions, we will only need to copy the system and vendor images to the corresponding partitions:

```sh
sudo dd if=system.img of=/dev/sdb2 bs=1M
sudo dd if=vendor.img of=/dev/sdb3 bs=1M
```

![07](images/run_rpi/07.png)

### Write boot partition

#### Get the latest Raspberry Pi firmware

We get the latest firmware from the following repository https://github.com/raspberrypi/firmware/archive/refs/heads/stable.zip. Once you extract the archive, you should get the following file structure:

![08](images/run_rpi/08.png)

Then we will get the following files from the repository:
- `boot/start.elf`
- `boot/fixup.dat`
- `boot/bootcode.bin`
- `boot/bcm2710-rpi-3-b-plus.dtb`
- `overlays/vc4-kms-v3d.dtbo`
- `overlays/vc4-kippah-7inch.dtbo`
- `overlays/rpi-ft5406.dtbo`

#### Edit the configuration

- Edit the file `config.txt` in order to use the device tree of a Raspberry Pi 3b+ model. You should modify the following line:

```
...
device_tree=bcm2710-rpi-3-b-plus.dtb
...
```

To write boot partition, we first need to mount it:

```sh
sudo mkdir /mnt/p1
sudo mount -t vfat /dev/sdb1 /mnt/p1
```

Then we can copy the required files:

```sh
sudo mkdir /mnt/p1/overlays

sudo cp boot/* /mnt/p1
sudo cp zImage /mnt/p1
sudo cp bcm2710-rpi-3-b.dts /mnt/p1
sudo cp vc4-kms-v3d-overlay.dts /mnt/p1/overlays/
sudo cp ramdisk.img /mnt/p1
```

After that we can unmount the boot partition:

```sh
sudo sync
sudo umount /mnt/p1
```

## Run the Android image

After flashing the SD card we can try to run our Android image by plugin it to the Raspberry Pi.

## Debug

To view the boot logs from a serial port on a Raspberry Pi, we need to enable UART. For that, we first need to check if our board supports the UART protocol.

```sh
strings bootcode.bin | grep BOOT_UART
```

If the string is found and the variable `BOOT_UART` is set to `0`, then we can enable debugging output through UART be settings the variable `BOOT_UART` to `1`.

```sh
sed -i -e "s/BOOT_UART=0/BOOT_UART=1/" bootcode.bin
```

# Liens

[bootcode.bin UART Enable](https://www.raspberrypi.com/documentation/computers/raspberry-pi.html#bootcode-bin-uart-enable)
