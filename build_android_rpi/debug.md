# Fix android audio version

## 1st attempt

> This may not prevent the kernel to boot

error to fix:

```
[   21.727386] init: Received control message 'interface_start' for 'android.hardware.audio@5.0::IDevicesFactory/default' from pid: 114 (/system/bin/hwservicemanager)
[   21.786367] init: Could not find 'android.hardware.audio@5.0::IDevicesFactory/default' for ctl.interface_start
[   21.827378] init: Received control message 'interface_start' for 'android.hardware.audio@4.0::IDevicesFactory/default' from pid: 114 (/system/bin/hwservicemanager)
[   21.860946] init: Could not find 'android.hardware.audio@4.0::IDevicesFactory/default' for ctl.interface_start
[   21.887168] init: Received control message 'interface_start' for 'android.hardware.audio.effect@5.0::IEffectsFactory/default' from pid: 114 (/system/bin/hwservicemanager)
[   21.927095] init: Could not find 'android.hardware.audio.effect@5.0::IEffectsFactory/default' for ctl.interface_start
```

The init process is trying to start the version 5.0 of the Android audio hardware whereas in the product Makefile (`device/brcm/rpi3`), we included the version 2.0:

```
# hardware/interfaces
PRODUCT_PACKAGES += \
    android.hardware.graphics.allocator@2.0-service.rpi3 \
    android.hardware.graphics.mapper@2.0-impl.rpi3 \
    android.hardware.graphics.composer@2.1-impl.rpi3 \
    android.hardware.audio@2.0-impl \
    android.hardware.audio.effect@2.0-impl \
    android.hardware.keymaster@3.0-impl \
    android.hardware.memtrack@1.0-service \
    android.hardware.memtrack@1.0-impl \
    android.hardware.wifi@1.0-service
```

In order to fix that, we will include the right version of the Android audio module and rebuild the sources. We sould have the following packages included:

```
# hardware/interfaces
PRODUCT_PACKAGES += \
    android.hardware.graphics.allocator@2.0-service.rpi3 \
    android.hardware.graphics.mapper@2.0-impl.rpi3 \
    android.hardware.graphics.composer@2.1-impl.rpi3 \
    android.hardware.audio@2.0-impl \
    android.hardware.audio.effect@2.0-impl \
    android.hardware.audio@5.0 \
    android.hardware.audio.effect@5.0 \
    android.hardware.keymaster@3.0-impl \
    android.hardware.memtrack@1.0-service \
    android.hardware.memtrack@1.0-impl \
    android.hardware.wifi@1.0-service
```

## 2nd attempt: deep dive in the audioserver implementation

> beep boop

## 3rd attempt: try to use generic audio

> Doesn't work

Set the variable `BOARD_USES_GENERIC_AUDIO` in the file `BoardConfig.mk` to disable audio support on the board and enable audio stub.

# Fix android surface flinger service

error to fix:

```
[   18.854224] init: starting service 'netd'...
[   18.868168] init: starting service 'zygote'...
[   18.868683] init: Created socket '/dev/socket/dnsproxyd', mode 660, user 0, group 3003
[   18.883473] init: Created socket '/dev/socket/zygote', mode 660, user 0, group 1000
[   18.894331] init: Created socket '/dev/socket/mdns', mode 660, user 0, group 1000
[   18.909512] init: Created socket '/dev/socket/usap_pool_primary', mode 660, user 0, group 1000
[   18.924436] init: Created socket '/dev/socket/fwmarkd', mode 660, user 0, group 3003
[   19.021202] init: starting service 'audioserver'...
[   19.044930] init: starting service 'surfaceflinger'...
[   19.063142] init: Failed to bind socket 'pdx/system/vr/display/client': No such file or directory
```

The kernel is stuck in a boot loop trying to start the surfacefligner service.

## 1st attempt: change the included version of opengl in the device Makefile

> Sounds good, doesn't work T.T

Currently, the included version of the opengles library is the version **2.0**:

```sh
# device/brcm/rpi3/rpi3.mk
PRODUCT_PROPERTY_OVERRIDES += \
    debug.drm.mode.force=1280x720 \
    gralloc.drm.device=/dev/dri/card0 \
    ro.opengles.version=131072 \
    ro.config.low_ram=true \
    wifi.interface=wlan0
```

Now, we will include the version **3.1**:

```sh
PRODUCT_PROPERTY_OVERRIDES += \
    debug.drm.mode.force=1280x720 \
    gralloc.drm.device=/dev/dri/card0 \
    ro.opengles.version=136609 \
    ro.config.low_ram=true \
    wifi.interface=wlan0
```

## 2nd attempt: prevent the creation of the socket pdx/system/vr/display/client

> Doesn't work either

The socket is created in the init script of surface flinger located at `frameworks/native/services/surfaceflinger/surfaceflinger.rc`:

```
service surfaceflinger /system/bin/surfaceflinger
    class core animation
    user system
    group graphics drmrpc readproc
    onrestart restart zygote
    writepid /dev/stune/foreground/tasks
    socket pdx/system/vr/display/client     stream 0666 system graphics u:object_r:pdx_display_client_endpoint_socket:s0
    socket pdx/system/vr/display/manager    stream 0666 system graphics u:object_r:pdx_display_manager_endpoint_socket:s0
    socket pdx/system/vr/display/vsync      stream 0666 system graphics u:object_r:pdx_display_vsync_endpoint_socket:s0
```

According to the official documentation of Android, we can disable vr support on Android by setting the following property on our build file `device/brcm/rpi3/rpi3.mk`:

```sh
PRODUCT_PROPERTY_OVERRIDES += \
    debug.drm.mode.force=1280x720 \
    gralloc.drm.device=/dev/dri/card0 \
    ro.opengles.version=131072 \
    ro.config.low_ram=true \
    ro.surface_flinger.use_vr_flinger=false \       <-- we added this property
    wifi.interface=wlan0
```

Informations on other properties of SurfaceFlinger can be found in the file `frameworks/native/services/surfaceflinger/sysprop/SurfaceFlingerProperties.sysprop`.

## 3rd attempt: include swiftshader library in the built system image

> May work

In order to include the swiftshader library, we need to add the following system property in the device Makefile `rpi3.mk`:

```sh
PRODUCT_PROPERTY_OVERRIDES += \
    debug.drm.mode.force=1280x720 \
    gralloc.drm.device=/dev/dri/card0 \
    ro.opengles.version=131072 \
    ro.hardware.egl=swiftshader \         <-- we added this config
    ro.config.low_ram=true \
    wifi.interface=wlan0
```

The mesa3d library may also work:

```sh
PRODUCT_PROPERTY_OVERRIDES += \
    debug.drm.mode.force=1280x720 \
    gralloc.drm.device=/dev/dri/card0 \
    ro.opengles.version=131072 \
    ro.hardware.egl=mesa3d \         <-- we added this config
    ro.config.low_ram=true \
    wifi.interface=wlan0
```

## 4th attemtpt: set the following properties to match the display dimensions

```
ro.hardware.gralloc=default
ro.hardware.egl=swiftshader
```

# Fix camera server not starting

error to fix:

```
[  242.507687] init: starting service 'media'...
[  242.523108] init: starting service 'wificond'...
[  245.825521] init: starting service 'netd'...
[  245.839504] init: starting service 'zygote'...
[  245.840025] init: Created socket '/dev/socket/dnsproxyd', mode 660, user 0, group 3003
[  245.854882] init: Created socket '/dev/socket/zygote', mode 660, user 0, group 1000
[  245.859269] init: starting service 'audioserver'...
[  245.865009] init: starting service 'surfaceflinger'...
[  247.491567] init: starting service 'cameraserver'...
[  247.505598] init: couldn't write 3419 to /dev/cpuset/camera-daemon/tasks: No such file or directory
```

## 1st attempt: remove the camera stub

> doesn't work

In the board configuration Makefile (`device/brcm/rpi3/BoardConfig.mk`), we will set the variable `USE_CAMERA_STUB` to false:

```sh
USE_CAMERA_STUB := false
```

# Fix missing audio hal

## 1st attempt: remove audio feature from the core xml definition

> doesn't work T.T

# Fix missing service implementation

## 1st attempt: fix missing audio HAL implementation by including the default one

[HIDL Audio HAL](https://source.android.com/docs/core/audio/hidl-implement)

# Results from tombstone

Android has a mecanism to trace crash event. All the logs are stored in the data partition, in the folder `/tombstone`.

## 1st bug from surfaceflinger: missing opengles implementation

Here is the bug from the surfaceflinger service:

```
*** *** *** *** *** *** *** *** *** *** *** *** *** *** *** ***
Build fingerprint: 'arpi/rpi3/rpi3:10/QQ2A.200305.004.A1/eng.root.20240628.143314:eng/test-keys'
Revision: '0'
ABI: 'arm'
Timestamp: 1970-01-01 00:00:29+0000
pid: 523, tid: 523, name: surfaceflinger  >>> /system/bin/surfaceflinger <<<
uid: 1000
signal 6 (SIGABRT), code -1 (SI_QUEUE), fault addr --------
Abort message: 'couldn't find an OpenGL ES implementation, make sure you set ro.hardware.egl or ro.board.platform'
    r0  00000000  r1  0000020b  r2  00000006  r3  bee30d38
    r4  bee30d4c  r5  bee30d30  r6  0000020b  r7  0000016b
    r8  bee30d48  r9  bee30d38  r10 bee30d68  r11 bee30d58
    ip  0000020b  sp  bee30d08  lr  a8ba6c67  pc  a8ba6c7a
```

solution: change the included opengl version:

```
PRODUCT_PROPERTY_OVERRIDES += \
    debug.drm.mode.force=1280x720 \
    gralloc.drm.device=/dev/dri/card0 \
    ro.opengles.version=196609 \
    ro.config.low_ram=true \
    wifi.interface=wlan0
```

## 2nd bug from surfaceflinger: cannot open /dev/dri/card0

the bug:

```
01-01 00:00:29.499   513   513 I SurfaceFlinger: Using HWComposer service: 'default'
01-01 00:00:29.500   513   513 I SurfaceFlinger: SurfaceFlinger is starting
01-01 00:00:29.505   513   513 I SurfaceFlinger: Treble testing override: 'false'
01-01 00:00:29.506   513   513 I SurfaceFlinger: SurfaceFlinger's main thread ready to run. Initializing graphics H/W...
01-01 00:00:29.506   513   513 I SurfaceFlinger: Phase offset NS: 1000000
01-01 00:00:29.508   513   540 W SurfaceFlinger: Ignoring VSYNC request while display is disconnected
01-01 00:00:29.508   513   540 W SurfaceFlinger: Ignoring VSYNC request while display is disconnected
01-01 00:00:29.508   513   541 W SurfaceFlinger: Ignoring VSYNC request while display is disconnected
01-01 00:00:29.509   513   541 W SurfaceFlinger: Ignoring VSYNC request while display is disconnected
01-01 00:00:29.509   513   513 D RenderEngine: RenderEngine GLES Backend
01-01 00:00:29.523   513   513 D libEGL  : loaded /vendor/lib/egl/libGLES_mesa.so
01-01 00:00:29.808   513   513 E GRALLOC-DRM: failed to open /dev/dri/card0
01-01 00:00:29.808   513   513 W EGL-MAIN: fail to get drm fd
01-01 00:00:29.809   513   513 W libEGL  : eglInitialize(0xa81ad600) failed (EGL_NOT_INITIALIZED)
```

- trying to change the value card0 to card1 in the file `rpi3.mk`:

> Doesn't work

```
PRODUCT_PROPERTY_OVERRIDES += \
    debug.drm.mode.force=1280x720 \
    gralloc.drm.device=/dev/dri/card1 \
    ro.opengles.version=196609 \
    ro.config.low_ram=true \
    wifi.interface=wlan0
```

- The kernel is missing direct support for the DRM VC4 driver so we need to ensure that the following kernel configurations are set and are not included as modules:

```
CONFIG_DRM=y -
CONFIG_DRM_VC4=y -
CONFIG_DRM_KMS_HELPER=y -
CONFIG_DRM_GEM_CMA_HELPER=y -
CONFIG_DRM_VC4_HDMI_CEC=y -
CONFIG_DRM_VC4_HVS=y
CONFIG_DRM_V3D=y -
```

- change the permissions of the DRM device node with **udev**