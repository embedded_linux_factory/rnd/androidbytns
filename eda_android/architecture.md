# Architecture
L'architecture d'un système Android se compose des couches suivantes:
- `Android app` une application qui n'utilise que l'API d'Android
- `Privileged app` une application qui utilise l'API d'Android et l'API Système
- `Device manufacturer app` une application qui utilise l'API d'Android, l'API Système et le FrameWork Android
- `System API` représente la partie de l'API Android disponible seulement pour les partenaires et les OEM (Original Equipment Manufacturer)
- `Android API` représente l'API publique d'Android
- `Android Framework` code précompilé en Java sur lequel se base les applications sous Android. Une portion du Framework est disponible depuis l'API publique et une autre depuis l'API système.
- `System services` Les services systèmes sont des composants destinés à effectuer des tâches particulières. Les fonctionnalités exposées par le Framework Android communiquent avec les services système pour avoir accès aux composants matériels.
- `Android runtime (ART)`: c'est un environnement d'exécution de Java spécifique à Android. l'ART est chargé de traduire le bytecode d'une application en instructions pre-processeurs qui seront exécutées par l'environnement d'exécution de la machine. l'ART a été introduite dans l'environnement Android depuis la version 4.4 "Kitkat" et a remplacé la `DVM (Dalvik VM)` depuis la version 5.0 "Lollipop"
- `HAL (Hardware Abstraction Layer)`: L'intérêt de cette couche est de faciliter la manipulation des composants matériels en définissant une interface standard que les constructeurs de matériel doivent implémenter.
- `Native daemons and libraries` les deamon et les librairies natives interagissent directement avec le kernel et n'ont pas besoins de manipuler la HAL pour interagir avec les composants matériels.
- `Kernel` le kernel Android est basé sur le kernel Linux et est séparé en modules spécifiques au constructeurs et en modules agnostiques au matériels. 

![schéma architecture AOSP](image/android-stack.svg)

Ces différentes couches peuvent être regroupées dans les composants suivants:
- `System Apps`
- `Java API Frameworks`
- `Native C/C++ Libraries`
- `Android Runtime`
- `HAL (Hardware Abstraction Layer)`
- `Kernel` 

![schéma architecture globale](image/android_platform.png)

# Liens

[android internals](https://null-android-pentesting.netlify.app/src/android-internals.html) 

[Android security (and not) Internals](https://www.researchgate.net/publication/292906634_Android_Security_and_Not_Internals) 

[android core docs](https://source.android.com/docs/core) 

[how does zygote forks DVM ?](https://stackoverflow.com/questions/9153166/understanding-android-zygote-and-dalvikvm) 

[Android developer codelab](https://source.android.com/docs/setup/start) 

[Patches du kernel Linux pour Android](http://www-igm.univ-mlv.fr/~dr/XPOSE2008/android/archi_linux.html) 

[New Android book](https://newandroidbook.com/)