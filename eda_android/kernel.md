# Kernel
Le kernel utilisé sous Android se base sur le kernel Linux LTS (Long Term support) avec un ensemble de patchs spécifiques au système d'exploitation. Ces systèmes dérivés du kernel Linux sont appelés les **ACK (Android Common Kernel)**. Les développeurs ont par la suite pris la décision de séparer les modules kernel génériques et les modules spécifiques aux constructeurs. Ces nouveaux systèmes sont les **GKI (Generic Kernel Image)**. Pour pouvoir interagir avec les modules spécifiques aux constructeurs les GKI se base sur des **KMI (Kernel Module Interface)** qui sont constitués d'une liste de symboles qui identifie les fonctions et les variables globales requises par les modules spécifiques aux constructeurs.

Voici un schéma qui illustre l'interaction entre un GKI et un module constructeur: 
![schéma GKI](image/generic-kernel-image-architecture.png)

## Principaux changements du Kernel
Étant basé sur le kernel Linux, les GKI ont gardé les fonctionnalités essentielles du kernel Linux mais ont dû s'adapter aux environnements mobiles disposant de ressources très limitées en ajoutant leur propre mécanismes et modifiant certains comportements existants. Les changements majeurs apportés sont les suivants:
- `Wakelocks`: Dans le but d'optimiser la consommation de la batterie d'un appareil mobile, les développeurs d'Android ont pris l'initiative de mettre tout le système en veille le plus souvent possible. Cependant certaines applications ont besoin de garder le système éveillé selon leur usage. Pour pallier à ce problème, les développeurs ont créer la fonctionnalité **Wakelock** qu'ils ont intégré au service système **PowerManager**. Cette fonctionnalité permet à une application effectuant des tâches à l'arrière-plan de demander au kernel de maintenir les CPU actifs. Pour qu'une application puisse utiliser cette fonctionnalité, elle doit dans un premier temps déclarer la permission dans son **manifest**:
```xml
<uses-permission android:name="android.permission.WAKE_LOCK" />
```
Puis une application peut acquérir une instance d'un **wake lock** en utilisant un broadcast receiver de la façon suivante en **Kotlin**:
```Kotlin
val wakeLock: PowerManager.WakeLock =
        (getSystemService(Context.POWER_SERVICE) as PowerManager).run {
            newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "MyApp::MyWakelockTag").apply {
                acquire()
            }
        }
```
et de la façon suivante en **Java**:
```Java
PowerManager powerManager = (PowerManager) getSystemService(POWER_SERVICE);
WakeLock wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
        "MyApp::MyWakelockTag");
wakeLock.acquire();
```
Il est cependant nécessaire de n'utiliser les **wakelock** qui si nécessaire à cause de leur consommation excessive de la batterie de l'appareil. Il existe plusieurs alternatives possibles en fonction des tâches à réaliser:
    - Si l'application doit réaliser des téléchargements en arrière-plan, il est recommandé d'utiliser le service **DownloadManager**.
    - Si l'application doit synchroniser de la donnée sur un serveur, il est recommandé d'utiliser un **sync adapter**.
    - Si l'application dépend d'un service qui tourne en tâche de fond, il est recommandé d'utiliser un **Job Scheduler**.
    - Si l'application doit garder l'écran éveillé, il est recommandé d'utiliser le flag **FLAG_KEEP_SCREEN_ON** dans une activité.
- `Binder`: Binder est le principal système de communication inter-processus du kernel Android. C'est par lui que passera toutes les communications avec les services d'Android. Ce mécanisme est à la base inspiré d'OpenBinder dont l'équipe de développeurs travaille maintenant chez Google.
- `Ashmem (Android shared memory)`: **Ashmem** est un mécanisme de shared memory qui remplace **SHM** sous Linux. Il a été implémenté par l'équipe des développeurs d'Android dans le but palier à la mauvaise gestion de la mémoire par le mécanisme **SHM**. Ce mécanisme de mémoire partagée est utilisé sous Android par le Binder dans le but de partager des quantités de données supérieurs à 1Mo. En effet, la taille des messages du Binder est limitées à 1Mo. Pour palier à cette limitation, il est possible de partager un descripteur de fichier vers une zone mémoire allouée grâce à **Ashmem** dans un message du Binder.
- `Alarm timer`: Toujours dans le but de répondre au besoin d'optimisation de la gestion des ressources, les développeurs d'Android on proposé leur propre version du Timer du kernel basé sur le timer qui permet au applications userland de demander au kernel de les réveillé à une heure donnée.
- `Low Memory Killer Daemon`: le daemon Low Memory Killer remplace le mécanisme d'OOM Killer sous Linux. En effet, dans le cas où la machine manque de mémoire physique, l'OOM Killer ne se base que sur son propre score pour tuer les processus les plus gourmands en mémoire. Cependant, ce mécanisme ne permet pas de prendre en compte la priorité des processus lancé.  Les développeurs d'Android ont donc créer leur propre daemon qui a pour but de tuer les processus trop gourmand en prenant en compte plus de métrique pour tuer dans un premier temps les tâches de fond non critiques, puis les applications en premier plan non essentielles au système.
- `paranoid networking`: Dans le but de renforcer le contrôle d'accès au réseau, les développeurs d'Android ont introduit la fonctionnalité paranoid networking dans le but de filtrer l'accès à certains composants réseaux en fonction des groupes des processus.

# Liens
[Overview  of the Linux Kernel](https://linux-kernel-labs.github.io/refs/heads/master/lectures/intro.html#overview-of-the-linux-kernel)

[The Linux Kernel in AOSP](https://www.youtube.com/watch?v=6O878RYYM18)

[Keep the device awake](https://developer.android.com/develop/background-work/background-tasks/scheduling/wakelock)

[Android power management](https://elinux.org/Android_Power_Management)

[Binder transactions in the bowels of the Linux Kernel](https://www.synacktiv.com/en/publications/binder-transactions-in-the-bowels-of-the-linux-kernel)

[Android Kernel Features](https://elinux.org/Android_Kernel_Features)

[Low Memory Killer Daemon](https://source.android.com/docs/core/perf/lmkd)