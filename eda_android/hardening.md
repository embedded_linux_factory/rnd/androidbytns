# Hardening

ideas on hardening:

- Find the most common attack pattern on the Android OS
- Find the features that are not included by default on the OS to prevent those attack pattern

## Fonctionnalités de sécurité à intégrer dans une image Android renforcée

**1. Renforcement du noyau Android:**

* Implémenter des mécanismes de sécurité renforcés tels que SELinux et TrustZone pour protéger le noyau contre les attaques sophistiquées.
* Limiter l'accès aux composants critiques du système uniquement aux applications et processus autorisés.
* Activer la vérification de signature par défaut pour tous les modules du noyau.

**3. Isolation des applications:**

* Utiliser des technologies telles que App Sandboxing pour isoler les applications les unes des autres et du système, limitant ainsi la propagation des malwares et des attaques.
* Restreindre les autorisations accordées aux applications pour minimiser leur accès aux données et ressources sensibles.
* Surveiller le comportement des applications pour détecter des activités suspectes.

**4. Chiffrement des données:**

* Activer le chiffrement complet de l'appareil pour protéger les données stockées, y compris les applications, les photos, les messages et les fichiers.
* Chiffrer les communications réseau pour protéger les données sensibles lors de leur transmission.
* Utiliser des clés de chiffrement fortes et les stocker en toute sécurité.

## Security recommendations

- Explain how to change SELinux mode to **Enforcing**
- Explain how to set up a trusted environment
- Enable full device encryption
- Enable strong authentication mecanisms (2FA)

## Take Graphene OS as an example

- Sandboxed Google Services for privacy

## Other examples

- Fushia OS: based on the zicorn kernel and has a complete different security model

## Existing hardened OS

There are some custom Android images that are focused on privacy and security. This include Emteria OS, Graphene OS and Linage OS. Their main advantage is that they don't include Google services that may be a threat for a company's or users privacy. They also include Android's main security feature to ensure a protection against Android's threats.

## Security features to integrate at the Kernel level

One of the main security feature to integrate at the hardware level is the encryption of the device. For that, a hardware backed keystore can be used. On Android devices, this is implemented as a Trusted Execution Environment (TEE).


## Liens

[Google security blog, Hardening Firmware Across the Android Ecosystem](https://security.googleblog.com/2023/02/hardening-firmware-across-android.html)
