# Enable KVM on Debian running on WSL2

On WSL2, nested virtualization is supported but not enabled by default. To enable it, you must:

1. Add yourself to the **kvm group**

```sh
sudo usermod -a -G kvm ${USER}
```

2. Change the default group of the `/dev/kvm` file

