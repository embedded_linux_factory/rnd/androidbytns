# HAL
Une HAL sous Android est une API utilisée par les services système et implémentée par les constructeurs en fonction des composants matériels utilisés. L'objectif de cette API est d'abstraire l'implémentation spécifique aux composants matériels aux couches logicielles.

Les HAL ont été introduites sous Android depuis la version 8.0. Elles étaient initialement définies dans le langage **HIDL (HAL Interface Definition Language)** et peuvent maintenant définies dans le langage **AIDL (Android Interface Definition Language)** depuis la version 11.0.

## Exemple d'utilisation: le son sous Android
Un exemple d'utilisation des HAL sous Android est la manipulation des composants dédiés au son. Pour qu'une application puisse manipuler les composants liés au son sous Android, elle doit passer par les étapes suivantes:
- **Framework Android**: Au plus haut niveau, l'application doit faire appel à l'API **android.media.\*** du **framework Android** pour pouvoir manipuler les composants sous-jacent.
- **JNI (Java Native Interface)**: Les classes du **framework Android** vont par la suite utiliser des classes de la **JNI**. la **JNI** est l'interface qui permet à un programme en Java et/ou en Kotlin de pourvoir faire appel à  des programmes plus bas niveau écrit en C/C++. Dans l'exemple de l'accès aux composants audio, la JNI va définir une interface vers le **framework natif** d'Android.
- **native framework**: Le **framework natif** définit une API équivalente au **framework Android** mais est écrit en C/C++. C'est lui qui sera chargé de faire appel au service système chargé du son, **audioflinger**.
- **binder IPC proxies**: Les proxy IPC du binder ont pour rôle de faciliter la communication entre les processus applicatifs et les services systèmes.
- **media server (system service)**: Le serveur de media contient plusieurs services systèmes dont le service audio qui utilisera l'interface définie par la HAL correspondant aux pilotes audio.
- **HAL**: La HAL définie l'interface qui sera utilisé par les services systèmes et qui devra être implémenter par les constructeurs dans le but d'utiliser leurs propres composants.
- **Kernel (driver)**: Enfin, au plus bas niveau, les pilotes des constructeurs liés au son seront utilisés par la HAL.

Voici un schéma récapitulatif de l'interaction entre les différentes couches présentées: 
![Android audio architecture](image/android_audio_architecture.png)

# Liens
[Introduction à la HAL Android](https://linuxembedded.fr/2014/05/introduction-a-la-hal-android) 

[Articles about HAL](https://www.sciencedirect.com/topics/computer-science/hardware-abstraction-layer) 

[Audio overview in Android core topics](https://source.android.com/docs/core/audio) 

[Audio HAL reference](https://android.googlesource.com/platform/hardware/interfaces/+/refs/heads/main/audio/)