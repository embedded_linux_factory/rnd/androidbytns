# Etat de l'art sur Android

Voici comment se structurent les différents fichiers présents dans ce dossier:
- [architecture](architecture.md): Description de l'architecture générale d'Android.
- [boot](boot.md): Description du processus de boot sous Android.
- [build](build.md): Description du processus de build d'une image Android.
- [art](art.md): Description du mécanisme d'interprétation de byte code de l'ART (Android RunTime) et du mécanisme de ramasse-miettes (Garbage Collection).
- [kernel](kernel.md): Présentation du kernel Android et des fonctionnalités qu'il apporte.
- [HAL](hal.md): Présentation des HAL et de leur utilisation sur Android.
- [security](security.md): Présentation de différentes couches de sécurité sous Android.