# Développement d'une image Android
## Prérequis pour faire tourner une image Android
Pour faire tourner une image d'Android, les prérequis matériels sont les suivants:
- Une architecture ARM, MISP ou x86 sur 32 ou 64 bits
- Avoir une version du Kernel Linux supérieure à la version 4.4
- 512 Mo de RAM
- Au moins 1 Go de stockage
- Une carte SD avec au moins 32Gio
- un écran tactile qui fait au minimum 2.2" par 3.4"
- une GPU avec la bibliothèque OpenGL ES 2.0

## Prérequis pour build une image Android
### Prérequis matériels
- Un système avec une architecture 64 bits.
- au moins 400 Go d'espace de stockage disponible pour récupérer les sources et les build
- un minimum de 64 Go de RAM disponible. Sur une machine 72 coeurs, le process de build peut prendre 40 min tandis qu'avec une machine de 6 coeurs, la phase de build peut prendre 6 heures.
- la machine de build doit tourner avec une distribution Linux qui supporte une version de la `glibc` supérieure à la version `2.17`.

### Paquets nécessaires
Pour build l'image les paquets suivants doivent être installé sur la machine de build:
git-core gnupg flex bison build-essential zip curl zlib1g-dev libc6-dev-i386 libncurses5 x11proto-core-dev libx11-dev lib32z1-dev libgl1-mesa-dev libxml2-utils xsltproc unzip fontconfig

### Software requis
Pour pouvoir build une image Android il est nécessaire de disposer des programmes `Python 3`, `Make`, `OpenJDK` et `Repo`. La branche principale du projet dispose déjà des programmes `Python 3`, `Make` et `OpenJDK` le seul programme a installer est donc `Repo`. Le programme peut s'installer avec un package manager:
```
sudo apt-get install update
sudo apt-get install repo
```
ou directement depuis les sources de google
```
export REPO=$(mktemp /tmp/repo.XXXXXXXXX)
curl -o ${REPO} https://storage.googleapis.com/git-repo-downloads/repo
gpg --recv-keys 8BB9AD793E8E6153AF0F9A4416530D5E920F5C65
curl -s https://storage.googleapis.com/git-repo-downloads/repo.asc | gpg --verify - ${REPO} && install -m 755 ${REPO} ~/bin/repo
```
Pour s'assurer de la bonne installation du programme, il est possible d'utiliser la commande suivante:
```
repo version
```

## Build une image Android
### Récupérer les sources
Récupérer les sources d'Android peut se faire grâce au packet `repo` mis à disposition par **Google**. Cela peut se faire en suivant ces étapes:
- Créer un dossier dans lequel sera cloné les sources:
```
cd ~
mkdir aosp && cd aosp
```
- Dans le dossier créé, récupérer les sources:
```
repo init -u https://android.googlesource.com/platform/manifest
```
- Par la suite la commande suivante permettra de synchroniser le répertoire avec les sources:
```
repo sync -j8
```
`Attention`: la synchronisation des sources peut prendre **1 heure ou plus**.

### Build de l'image
Le build system utilisé pour compiler Android est Soong. Celui ci remplace GNU Make depuis la version 7.0. Il est cependant à noté que la migration de CMake à Soong n'est pas encore complète c'est pourquoi il existe encore des Makefiles intégrés aux fichiers Blueprint.
Une fois les sources récupérée, le projet peut être compilé en initialisant l'environnement de build avec la commande suivante:
```sh
source build/envsetup.sh
```
puis en spécifiant l'architecture cible avec la commande suivante:
```sh
lunch <target>
```
et en lançant la phase de build avec la commande:
```sh
m
```

Il est possible de définir une nouvelle architecture cible dans le dossier `device/[organisation]/[matériel]`

### Customisation de l'image
Pour créer une image d'Android personnalisée pour une plateforme spécifique, il est nécessaire de créer un nouveau paquet pour supporter la plateforme. Ce paquet sera constitué de la configuration de la machine, de plusieurs HAL et d'autres paquets système.

# Liens

[AOSP docs: Build Android](https://source.android.com/docs/setup/build/building) 

[Set up for AOSP development (9.0 or later)](https://source.android.com/docs/setup/start/requirements) 

[Android 14 Compatibility Definition](https://source.android.com/docs/compatibility/14/android-14-cdd) 

[How to Install Android on Raspberry Pi in 2024](https://raspberrytips.com/android-raspberry-pi-4/)