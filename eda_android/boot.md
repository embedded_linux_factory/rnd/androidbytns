# Séquence de démarrage sous Android

La séquence de démarrage sous Android dépend fortement des spécifications des constructeurs. En effet, le premier composant chargé pendant la phase de démarrage est le **ROM code** du constructeur du processeur.

Dans la séquence de démarrage d'un système Android, les premiers composants chargés en mémoire sont dépendants du constructeur du processeur. La séquence de démarrage la plus rencontrée est la suivante:

1. Une **ROM code** va charger le **bootloader** en mémoire. Dans le cas d'un environnement embarqué, le bootloader réellement chargé est un **SPL (Secondary Program Loader)**. Le **SPL** étant chargé dans un environnement restreint, son rôle sera d'initialiser les composants matériels nécessaires pour charger le **TPL (Ternary Program Loader)**. Le **TPL** sera par la suite chargé de lancé le **kernel** et de lui laisser la main.

2. Une fois le kernel chargé en mémoire, son rôle est d'initialiser les composants nécessaires pour pouvoir exécuter des applications **userland** et de lancer le processus **init**.

3. Contrairement aux distributions Linux les plus utilisées, les développeurs d'Android ont réécrit leur propre version du programme **init**. Son rôle sera de lire le fichier **init.rc** qui importera d'autres script d'initialisation. Chaque script d'initialisation contiendront des actions à exécuter et des services à lancer.

4. Après avoir initialiser un environnement d'exécution userland nécessaire, le process **init** va créer une instance de l'ART qui va à son tour créer une instance du processus **Zygote** qui sera le père de toutes les applications lancée sous Android.

5. Une fois lancé, le premier programme lancé par **Zygote** est le **System Server** qui aura pour rôle de démarrer tous les services systèmes nécessaires à l'exécution d'Android.

6. Un des services essentiels lancé par le **System Server** est l'**Activity Manager**. Son rôle sera de lancer toutes les applications utilisateur en faisant un fork du processus Zygote à chaque fois qu'un utilisateur clique sur une icône.

Voici un schéma qui résume le processus de démarrage qui vient d'être décrit: 
![Android boot process diagram](image/android_bootprocess.png)

# Liens
[Manage boot time](https://source.android.com/docs/automotive/power/boot_time) 

[Android Boot Flow](https://source.android.com/docs/security/features/verifiedboot/boot-flow) 

[Android Booting](https://elinux.org/Android_Booting) 

[I hack, U-Boot](https://www.synacktiv.com/en/publications/i-hack-u-boot) 

[First-Stage Loaders: BIOS, [U]EFI, iBoot1 & U-Boot (SPL)](https://medium.com/@tunacici7/first-stage-loaders-bios-u-efi-iboot1-u-boot-spl-5c0bee7feb15) 

[wikipedia: Booting process of Android devices](https://en.wikipedia.org/wiki/Booting_process_of_Android_devices) 

[How to set up a bootloader for an embedded linux machine](https://www.msystechnologies.com/blog/how-to-setup-a-bootloader-for-an-embedded-linux-machine/) 

[TPL: SPL loading SPL](pdf/tpl-presentation.pdf) 

[Understanding the (Embedded) Linux boot process](https://kleinembedded.com/understanding-the-embedded-linux-boot-process/) 

[RPI: Second stage bootloader](https://www.raspberrypi.com/documentation/computers/raspberry-pi.html#boot-sequence) 

[Wikipedia: init](https://en.wikipedia.org/wiki/Init) 

[Comparison of init systems](https://wiki.gentoo.org/wiki/Comparison_of_init_systems) 

[Android startup tour - init](https://blog.ngzhian.com/android-startup-tour-init.html) 

[Android Init Language](https://android.googlesource.com/platform/system/core/+/master/init/README.md) 

[Android Boot Process Step-by-Step (All Stages in Booting Process)](https://www.tutorialsfreak.com/app-penetration-testing-tutorial/android-boot-process) 

[Android OS boot process with focus on Zygote](https://www.codecentric.de/wissens-hub/blog/android-zygote-boot-process)