# Android security model

The security model on the Android's operating system is closely related to its architecture model. Each layer must guarantee the security at its level and assume that the underlying layers guarantee their own security as well. In this document, we will take a close look at each security feature lying in the different layers of the Android operating system.

## Hardware layer

### TEE

At the hardware layer, Android leverages underlying components by providing security features to access and store sensitive data. For that, Android supports a Trusted Execution Environment (TEE) based on ARM TrustZone. This TEE is responsible for handling any operation involving critical data. This environment consists of a separate operating system isolated from the Android Linux Kernel by the processor. On Google's Android device, the trusted operating system running along with Android is `Trusty`, an open source TEE which provide the required features for a mobile device.

### Verified boot process

In addition of supporting a trusted environment, Android supports a verified boot process to ensure that any code running on the machine comes from a trusted source and not from an attacker or a compromission. In order to acheive that, a chain of trust is established from a hardware protected root of trust, the bootloader, the boot partition and other vendor-specific partitions. During device boot-up, each stage verifies the integrity and authenticity of the next stage before handing over execution.

### Keystore system

Android supports a keystore system that relies on a hardware-backed container. This container can be a TEE or a dedicated secure component. Its main goal is to store confidential cryptographic keys and to guarantee that access restriction policies cannot be overpassed. This feature has been improved since Android 9 that was the first version to support the StrongBox KeyMint, a dedicated secure hardware component that ships with its own CPU, a secure storage, a true random number generator, and additional mechanisms to resist package tampering and unauthorized sideloading of apps.

`Keystore Key Attesation`: Keystore Key Attestation, required for devices launched with Android 8.0 and higher, allows servers to verify device and key properties, ensuring device integrity and enabling enterprise customers to manage device inventories and security status effectively.

`KeyChain`: The KeyChain class provides access to private keys and their corresponding certificate chains in credential storage. Whereas the KeyStore is for non-shareable app-specific keys, KeyChain is for system- or profile-wide keys that may be used by multiple apps with the approval of the EMM DPC or the user.

`Key Decryption on Unlocked Devices`: Android 9 and higher use the unlockedDeviceRequired flag to ensure that specified keys can only be used when the device is unlocked, enhancing the security of sensitive on-disk data by preventing decryption if the device is lost or stolen.

`Version Binding (Anti-Rollback Protection)`: All keys stored on the Keystore system are bound to the security patch level of the Android image. This ensures that an attacker who discovers a weakness in an old version of the Android system or TEE software cannot roll a device back to a vulnerable version and compromise secrets by using those vulnerabilities.

`User Authentication`: Android has adopted a tired model to classify authentication methods. This model is composed of 3 tiers: what you know (1st tier), what you are (2nd tier) and what you have (3rd tier). For the first tier, user authentication can be realised with the Gatekeeper or the Weaver subsystem in a TEE and for the second tier, Android provides the BiometricPrompts API that allows the usage of fingerprint or face authentication.

### Memory safety

The last security feature handled at the hardware level is the memory safety. For that, Android has developed several technologies: HWASan for pre-release testing, GWP-ASan and KFENCE for probabilistic detection in production. Moreover, as memory safety bugs mostly come from low-level languages like C and C++, Android developers start to integrate more modern language like Rust that adds some static and dynamic memory checks.

## Operating system Layer

At the operating system layer, Android adopts a defense-in-depth security model by taking advantage of the security features provided by the Linux Kernel. The key components of this security model are the kernel sandboxing that restricts kernel actions and userland access to kernel entry point, the process sandboxing that restrict the interactions with all the system processes and the application sandboxing that isolate applications from each other by assining a UID to each process and to apply restrictions based on SELinux. Moreover, Android employs several anti-exploitation measures, including Control Flow Integrity, Integer Overflow Sanitization, and new compiler-based mitigations to prevent bugs from becoming vulnerabilities. Android 10 introduced BoundsSanitizer for array bounds checks, and Clang’s UBSan for integer overflow checks, hardening components like the media framework against memory corruption. Additionally, Scudo, a hardened memory allocator, is used to detect and prevent memory safety errors such as use-after-free and bounds violations.

In addition to the mitigations added at the OS layer, Android enables some fine grained restriction features to enhance user's privacy.

## Network security

At the network layer, the key point concerning security is the integrity and the confidentiality of the data transferred. For that, Android has adopted some measures to protect transitting data that comes to and from a device.

The first measure adopted was the default use of TLS 1.3 to encrypt all communications of a device. Moreover, Android leverages the use of TLS by allowing the user and the IS administrators to enable DNS over TLS and enchance communications privacy.

In addition to TLS, Android has introduced some features to mitigate cellular connectivity vulnerabilities. Moreover, from Android 12, users and administrators now have an option to disable 2G connections to protect against attacks on an obsolete cellular communication model. Android has also introduced some security features in order to ensure that WIFI networks must be encrypted and authenticated.

Finally, to furthermore enchance privacy on Android's device the system developpers has implemented an isolated OS that can by used to run applications requiring stronger confidentiality and integrity. This separated OS is called Microdroid and is running alongside with the main Android OS as a virtual machine powered by the Android Virtualization Framework (AVF).

## Application security

At the application layer, Android must ensure that all applications that target its ecosystem don't have a harmful behaviour for the user. For that, Google provides services to help applications developpers and users to guanrantee a high level of protection.

To start with, Google Security Services provide robust protection for Android devices through components like Google Play Protect and the Play Integrity API. Google Play Protect is an always-on service that scans over 3.5 billion devices daily to detect and remove potentially harmful applications (PHAs). The Play Integrity API helps developers verify that interactions with their apps come from legitimate sources, reducing the risk of fraud and abuse. Enterprise Mobility Management (EMM) partners can use these services to ensure users only install apps from trusted sources, enhancing overall device security.

In addition to these services, Jetpack Security plays a crucial role by leveraging the Android KeyStore system to offer secure key management for developers. It provides tools like MasterKeys for generating secure AES 256 GCM keys and higher-level cryptographic abstractions such as EncryptedFile and EncryptedSharedPreferences. These features help developers securely store sensitive data, whether for enterprise, public, or private apps, thereby strengthening data protection across the board.

Furthermore, application signing is a critical security measure in Android, requiring all apps to be digitally signed with a developer key before installation. Android supports APK key rotation, allowing apps to update their signing keys while maintaining trust through an attestation chain. The introduction of APK signature scheme v3.1 in Android 13 addresses some key rotation issues and provides enhanced security features. Apps signed with the same key or part of the same signing lineage can share data and code securely, facilitating interoperability while maintaining security.

Complementing these efforts, Google Play Protect serves as a comprehensive threat detection service built into Google Play-enabled devices. It actively scans for malware and security risks, notifying users and taking automatic action if necessary. Play Protect also extends its protections to apps installed from sources outside Google Play, performing real-time threat detection to safeguard users against emerging threats. This service is crucial for maintaining the integrity of the Android ecosystem, ensuring that even sideloaded apps are scrutinized for malicious behavior.

Moreover, the Google Play Store employs strict policies and review processes to protect users from malicious applications. Apps submitted to the Play Store undergo rigorous checks to detect PHAs and other security threats. This proactive approach helps maintain a safe app environment, ensuring that users can download and use apps with confidence. The app review process is a key component in Google's strategy to secure the Android platform and its users from potential threats.

In conclusion, these components collectively outline a robust framework for application security on Android, leveraging Google's extensive security services and tools to protect users and their data from a wide array of threats. Through continuous innovation and stringent security measures, Android remains committed to providing a secure and reliable environment for all its users.

## Data Protection

Android is dedicated to protecting user data through strong encryption practices, innovative solutions like Adiantum, and secure backup encryption methods.

Android requires encryption to protect user data if a device is lost or stolen. The platform uses file-based encryption (FBE), allowing different directories to be encrypted with unique keys. There are two main types of storage:

- Device Encrypted (DE) Storage: Accessible once the device boots, before the user unlocks it. It’s protected by hardware secrets and software in the Trusted Execution Environment (TEE), ensuring that Verified Boot is successful before data decryption.
- Credential Encrypted (CE) Storage: Accessible only after the user unlocks the device. CE keys, safeguarded against brute force attacks, require user credentials to unlock.

Most apps store data in CE storage, becoming operational after the user enters their credentials. Some apps, like alarm clocks or accessibility services, can use Direct Boot APIs to function with DE storage before credential entry. Each user on a device has unique CE and DE keys, with CE keys protected by the user’s lockscreen credentials. These encryption keys are 256 bits long and generated randomly on-device. Metadata encryption adds an extra layer of security, protecting filesystem metadata through Keymaster and Verified Boot.

Adiantum is designed for devices running Android 9 and higher with CPUs that lack AES instructions. It provides strong encryption with minimal performance impact, enabling lower-powered devices to use robust encryption. Adiantum is mandatory for all new Android devices according to the Android Compatibility Definition Document (CDD).

Devices running Android 9 and higher support end-to-end encrypted backups. The backup data is encrypted on the device using a unique key, ensuring the backup server cannot decrypt the archive. This backup encryption key is further protected with a hash of the user’s lockscreen credentials and securely shared with a cohort of secure enclaves across Google’s data centers. These enclaves enforce strict limits on incorrect attempts, ensuring that only users with the correct PIN, pattern, or password can access their backed-up data.

Android's approach to data protection ensures that user data is secure both on-device and in the cloud. With advanced encryption techniques, including the innovative Adiantum for low-powered devices and robust backup encryption, Android leads the way in mobile security, ensuring that user information remains protected at all times.

# Liens

[Android Security Paper 2023](pdf/android-enterprise-security-paper-2023.pdf)

[Lockscreen and Authentication Improvements in Android 11](image/lockscreen_and_auth_improvements.png)

[Gatekeeper](https://source.android.com/docs/security/features/authentication/gatekeeper)

[Hardware-assisted AddressSanitizer Design Documentation](https://clang.llvm.org/docs/HardwareAssistedAddressSanitizerDesign.html)

[Queue the Hardening Enhancements](https://android-developers.googleblog.com/2019/05/queue-hardening-enhancements.html)

[open-tee](https://open-tee.github.io/android/)
