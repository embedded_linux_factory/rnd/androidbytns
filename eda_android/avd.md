# Run an Android virtual device

## Get a Raspberry Pi OS image for Qemu (optional)

### Install Qemu for an Arch Linux based ditribution

- Install `qemu` with the following command as **root**:
```sh
$ pacman -S qemu-full
```

### Install Qemu for a Debian based ditribution

- Install `qemu` with the following command as **root**:
```sh
$ apt-get install qemu-system
```

### Get the official Raspberry Pi OS image

> The image that will be used in this tutorial is the **Raspberry Pi OS Lite** released the **15th March 2024**. So to simply the tutorial, we will export the image name as follow:
> ```
> $ export RASPIOS_IMG='2024-03-15-raspios-bookworm-arm64-lite.img'
> ```

To get the official Raspberry Pi OS image, you can go to the official download page of raspberry (cf. [Raspberry Pi OS download page](https://www.raspberrypi.com/software/operating-systems/)). After getting the Raspberry Pi OS image, we can extract it:

```sh
$ xz -d "$RASPIOS_IMG"
```

### Mount the image

After we get the image, we can check what partitions are inside:

```sh
$ fdisk -l $RASPIOS_IMG

Disk 2024-03-15-raspios-bookworm-arm64-lite.img: 8 GiB, 8589934592 bytes, 16777216 sectors
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: dos
Disk identifier: 0xfb33757d

Device                                      Boot   Start     End Sectors  Size Id Type
2024-03-15-raspios-bookworm-arm64-lite.img1         8192 1056767 1048576  512M  c W95 FAT32 (LBA)
2024-03-15-raspios-bookworm-arm64-lite.img2      1056768 5406719 4349952  2.1G 83 Linux
```

We can see that the image contains 2 partitions:
- a **boot partition** starting at sector **8192**
- a **Linux partition** starting at sector **1056768**

The one we will mount is the boot partition. For that we need to get the **offset** of the partition. We can compute it by multiplying the **start sector (8192)** with the **sector size (512)**. So in our case, the offset size is 8192 * 512 = 4194304. So we can mount the partition with the following commands as root:

```sh
$ mkdir /mnt/image
$ mount -o loop,offset=4194304 "$RASPIOS_IMG" /mnt/image/
```

After that, we can list the content of the `/mnt/image/` folder.

```sh
$ ls -ls /mnt/image

total 62324
   32 -rwxr-xr-x 1 root root    31401 Mar  7 15:51 bcm2710-rpi-2-b.dtb
   34 -rwxr-xr-x 1 root root    33593 Mar  7 15:51 bcm2710-rpi-3-b.dtb
   34 -rwxr-xr-x 1 root root    34228 Mar  7 15:51 bcm2710-rpi-3-b-plus.dtb
   32 -rwxr-xr-x 1 root root    31312 Mar  7 15:51 bcm2710-rpi-cm3.dtb
   32 -rwxr-xr-x 1 root root    32570 Mar  7 15:51 bcm2710-rpi-zero-2.dtb
   32 -rwxr-xr-x 1 root root    32570 Mar  7 15:51 bcm2710-rpi-zero-2-w.dtb
   54 -rwxr-xr-x 1 root root    54813 Mar  7 15:51 bcm2711-rpi-400.dtb
   54 -rwxr-xr-x 1 root root    54809 Mar  7 15:51 bcm2711-rpi-4-b.dtb
   56 -rwxr-xr-x 1 root root    55449 Mar  7 15:51 bcm2711-rpi-cm4.dtb
   38 -rwxr-xr-x 1 root root    38349 Mar  7 15:51 bcm2711-rpi-cm4-io.dtb
   52 -rwxr-xr-x 1 root root    52228 Mar  7 15:51 bcm2711-rpi-cm4s.dtb
   76 -rwxr-xr-x 1 root root    77739 Mar  7 15:51 bcm2712d0-rpi-5-b.dtb
   76 -rwxr-xr-x 1 root root    77755 Mar  7 15:51 bcm2712-rpi-5-b.dtb
   76 -rwxr-xr-x 1 root root    77699 Mar  7 15:51 bcm2712-rpi-cm5-cm4io.dtb
   76 -rwxr-xr-x 1 root root    77691 Mar  7 15:51 bcm2712-rpi-cm5-cm5io.dtb
   52 -rwxr-xr-x 1 root root    52476 Mar 15 16:00 bootcode.bin
    2 -rwxr-xr-x 1 root root      154 Mar 15 16:08 cmdline.txt
    2 -rwxr-xr-x 1 root root     1213 Mar 15 16:00 config.txt
    4 -rwxr-xr-x 1 root root     3204 Mar 15 16:00 fixup4cd.dat
    6 -rwxr-xr-x 1 root root     5434 Mar 15 16:00 fixup4.dat
   10 -rwxr-xr-x 1 root root     8423 Mar 15 16:00 fixup4db.dat
   10 -rwxr-xr-x 1 root root     8425 Mar 15 16:00 fixup4x.dat
    4 -rwxr-xr-x 1 root root     3204 Mar 15 16:00 fixup_cd.dat
    8 -rwxr-xr-x 1 root root     7303 Mar 15 16:00 fixup.dat
   12 -rwxr-xr-x 1 root root    10268 Mar 15 16:00 fixup_db.dat
   12 -rwxr-xr-x 1 root root    10268 Mar 15 16:00 fixup_x.dat
10836 -rwxr-xr-x 1 root root 11095725 Mar 15 16:08 initramfs_2712
10836 -rwxr-xr-x 1 root root 11095386 Mar 15 16:08 initramfs8
    2 -rwxr-xr-x 1 root root      145 Mar 15 16:08 issue.txt
 9048 -rwxr-xr-x 1 root root  9264394 Mar 15 16:00 kernel_2712.img
 9044 -rwxr-xr-x 1 root root  9259827 Mar 15 16:00 kernel8.img
    2 -rwxr-xr-x 1 root root     1594 Mar 15 16:00 LICENCE.broadcom
   28 drwxr-xr-x 2 root root    28672 Mar 15 16:00 overlays
  790 -rwxr-xr-x 1 root root   808892 Mar 15 16:00 start4cd.elf
 3666 -rwxr-xr-x 1 root root  3753480 Mar 15 16:00 start4db.elf
 2204 -rwxr-xr-x 1 root root  2256224 Mar 15 16:00 start4.elf
 2934 -rwxr-xr-x 1 root root  3004040 Mar 15 16:00 start4x.elf
  790 -rwxr-xr-x 1 root root   808892 Mar 15 16:00 start_cd.elf
 4714 -rwxr-xr-x 1 root root  4825352 Mar 15 16:00 start_db.elf
 2912 -rwxr-xr-x 1 root root  2980544 Mar 15 16:00 start.elf
 3642 -rwxr-xr-x 1 root root  3727656 Mar 15 16:00 start_x.elf
```

In order to run **Qemu**, we will need the kernel and device tree. We can copy them outside of the `/mnt/image` forlder:

```sh
cp /mnt/image/bcm2710-rpi-3-b-plus.dtb /mnt/image/kernel8.img .
```

### Set up SSH

To connect to our emulated board, we can create a new password for the default `rpi` user by putting a new entry in the `userconf` file in the boot partition. For that, we need to specify the user name and the associated hashed password. To get the hash of a password, we can use the following command:

```sh
$ openssl passwd -6

Password:
Verifying - Password:
$6$eGUH...<your hashed password here>...pZ1
```

Then we can add the following entry in the `userconf` file:

```sh
$ echo 'pi:<your hashed password here>' | sudo tee /mnt/image/userconf
```

After that, we can enable ssh by creating a ssh file in the boot partition:

```sh
$ touch /mnt/image/ssh
```

### Running the image

Now that we have set up an ssh server, we can run the Raspberry Pi OS image that we got but first we need to resize it in order to comply with `qemu`'s requirements:

```sh
qemu-img resize -f raw "$RASPIOS_IMG" 8G
```

Then we can finally run the image with:

```sh
$ qemu-system-aarch64 \
    -machine raspi3b \
    -cpu cortex-a72 \
    -dtb bcm2710-rpi-3-b-plus.dtb \
    -m 1G \
    -smp 4 \
    -kernel kernel8.img \
    -sd "$RASPIOS_IMG" \
    -append "rw earlyprintk loglevel=8 console=ttyAMA0,115200 dwc_otg.lpm_enable=0 root=/dev/mmcblk0p2 rootdelay=1" \
    -device usb-net,netdev=net0 \
    -netdev user,id=net0,hostfwd=tcp::2222-:22 \
    -nographic
```

After that, we can connect to the image with ssh:

```sh
$ ssh -p 2222 pi@localhost

pi@localhost's password:
Linux raspberrypi 6.6.20+rpt-rpi-v8 #1 SMP PREEMPT Debian 1:6.6.20-1+rpt1 (2024-03-07) aarch64

The programs included with the Debian GNU/Linux system are free software;
the exact distribution terms for each program are described in the
individual files in /usr/share/doc/*/copyright.

Debian GNU/Linux comes with ABSOLUTELY NO WARRANTY, to the extent
permitted by applicable law.
pi@raspberrypi:~ $
```

## Run a Cuttlefish instance

Cuttlefish is a virtual Android device that let platform and application developpers be independent from hardware.

### Install Cuttlefish

> It is recommended to install Cuttlefish on a Debian based distribution. In this tutorial, we will install Cuttlefish in a **virtual machine running Debian 12**.

As Cuttlefish is a virtual device, it requires virtualization to be enabled. To check if virtualization is enable on a Linux machine, run the following command:

```sh
cat /proc/cpuinfo | grep -c -w "vmx\|svm"
```

It should **return a non 0 value**.

> to enable virtualization in a vmware instance on Windows, make sure the check the box `virtualize intel vt-x/ept or amd-v/rvi` and to check that Hyper-v features are not activated.

Once the host of Cuttle fish is ready, you can build and install the Debian packages associated with Cuttlefish with the following commands:

```sh
sudo apt install -y git devscripts equivs config-package-dev debhelper-compat golang curl
git clone https://github.com/google/android-cuttlefish
cd android-cuttlefish
for dir in base frontend; do
  pushd $dir
  # Install build dependencies
  sudo mk-build-deps -i
  dpkg-buildpackage -uc -us
  popd
done
sudo dpkg -i ./cuttlefish-base_*_*64.deb || sudo apt-get install -f
sudo dpkg -i ./cuttlefish-user_*_*64.deb || sudo apt-get install -f
sudo usermod -aG kvm,cvdnetwork,render $USER
sudo reboot
```

### Get a prebuild Android image for Cuttlefish

As Cuttlefish is part of the AOSP project some prebuilt images are made available from the Google developpers. All this images are available at the [Android Continuous Integration site](http://ci.android.com/).

To find a build suitable for a x86 based machine, you can go to the `aosp-main` branch and select the `aosp_cf_x86_64_phone` target.

![01](image/build_qemu/01.png)

You can Then select the build and search for the files `aosp_cf_x86_64_phone-img-xxxxxx.zip` and `cvd-host_package.tar.gz` in the **Artifacts** tab. This files are respectively the Android image built for Cuttlefish and the additional host packages to run it.

![02](image/build_qemu/02.png)

### Running the Cuttlefish image

Once you got the required packages and an appropriate Android image, you can extract them in a folder on your host then launch The image with Cuttlefish.

```sh
mkdir cf
cd cf
tar -xvf /path/to/cvd-host_package.tar.gz
unzip /path/to/aosp_cf_x86_64_phone-img-xxxxxx.zip

# lanches the Android image as a deamon
HOME=$PWD ./bin/launch_cvd --daemon
```

After we lanched the Android image, we can check if it is running with the `adb` binary that comes packed with the Cuttlefish packages:

```sh
# In the directory where you extracted your extra Cuttlefish packages
./bin/adb devices
```

Then we can interract with our image by accessing the Cuttlefish's web interface exposed by default at `https://localhost:8443`.

Finally, after we checked that our Android image is correctly running, we can stop it with the following command:

```sh
# In the directory where you extracted your extra Cuttlefish packages
HOME=$PWD ./bin/stop_cvd
```

# Links

[Raspberry Pi OS download page](https://www.raspberrypi.com/software/operating-systems/)

[Cuttlefish Virtual Android Devices](https://source.android.com/docs/devices/cuttlefish)
